'use strict';

<%= appName %>.controller = {
    redirectToHome: function () {
        Backbone.history.navigate('home', { trigger: true, replace: true });
    }
};
<%= appName %>.router = new Backbone.Marionette.AppRouter({
    controller: <%= appName %>.controller,
    appRoutes: {
        "": "redirectToHome"
    }
});

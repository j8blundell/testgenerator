'use strict';
(function(Backbone){


    Backbone.Marionette.TemplateCache.prototype.compileTemplate = function (rawTemplate) {
        return Handlebars.compile(rawTemplate);
    };

    var <%= appName %> = (function (Marionette) {
        <%= appName %> = new Marionette.Application();

        <%= appName %>.addRegions({
            mainRegion: '#main-region'
        });
        <%= appName %>.on("start", function (options) {

            Backbone.history.start({ pushState: true });

            <%= appName %>.vent.on('all', function (e) {
                if (typeof console !== 'undefined' && console.log) {
                    console.log('event', e);
                }
            });
            console.log('<%= appName %> has started');
        });

        return <%= appName %>;

    })((Backbone.Marionette));

})((var Backbone = Backbone || {}));

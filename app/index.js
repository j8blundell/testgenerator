var bmfmvc = require('yeoman-generator'),
    Promise = require('promise');

module.exports = bmfmvc.Base.extend({



    welcome: function() {
        console.log(this.yeoman);
    },

    creatingScaffold: function () {
        var done = this.async();
        this.dest.mkdir('Scripts/app');
        done();
    },
    promptUser: function() {
        var done = this.async();

        this.prompt({
            name: 'appName',
            message: 'What should your app be called ?',
            default: 'app'
        }, function (props) {
            this.appName = props.appName;
            done();
        }.bind(this));

    },


    install: function(){

        //var done = this.async(),
        //    gen = this;
        //
        //var getUrl = function(url){
        //    return new Promise(function(resolve, reject){
        //        gen.fetch(url, 'Scripts', function (err) {
        //            if(err){
        //                reject(err);
        //            } else{
        //                resolve("Stuff worked!");
        //            }
        //        });
        //    });
        //};
        //
        //getUrl('http://backbonejs.org/backbone.js').then(function(){
        //    return getUrl('http://marionettejs.com/downloads/backbone.marionette.js');
        //}).then(function(){
        //    return getUrl('http://code.jquery.com/jquery-1.11.1.js');
        //}).then(function(){
        //    return getUrl('http://underscorejs.org/underscore.js');
        //}).then(function(){
        //    done();
        //}).catch(function(err) {
        //    console.log('there was an error downloading a file: ', err);
        //    done();
        //});

    },
    writing: function(){
        var done = this.async();
        this.template('_app.js', 'Scripts/app/app.js');
        this.template('_router.js', 'Scripts/app/router.js');
        this.template('_initialize.js', 'Scripts/app/initialize.js');
        this.src.copy('_index.html', 'index.html');
        this.src.copy('_appSpec.js', 'specs/appSpec.js');
        this.template('_package.json', 'package.json');
      //  this.template('Gruntfile.js', 'Gruntfile.js');
        this.gruntfile.insertConfig("pkg", "grunt.file.readJSON('package.json')");

        this.gruntfile.insertConfig("jasmine", "{  pivotal: { src: ['Scripts/**/*.js'], helpers: ['bower_components/**/*.js'], options: {specs: 'specs/*Spec.js',helpers: 'spec/*Helper.js'  }  }  }");
        this.gruntfile.loadNpmTasks("grunt-contrib-jasmine");
        this.gruntfile.registerTask('test', 'jasmine');
        this.gruntfile.registerTask('default', 'jasmine');
        done();

        this.bowerInstall('marionette');
    },
    installingGrunt: function() {
        var done = this.async();
        this.npmInstall(['grunt'], { 'saveDev': true }, done);
    },
    installingJasmine: function() {
        var done = this.async();
        this.npmInstall(['grunt-contrib-jasmine'], { 'saveDev': true }, done);
    }



});